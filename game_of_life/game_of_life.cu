#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <ctime>
#include <cassert>

const bool gpu_mode = true;
const int width = 2048;

void swap(void* a, void* b) {
    long* la = (long*)a;
    long* lb = (long*)b;

    long tmp = *la;
    *la = *lb;
    *lb = tmp;
}

void init_grid(bool* grid) {
    for (int i = 0; i < width*width; i++) {
        grid[i] = false;
    }

    for (int y = 1; y < width-1; y++) {
        for (int x = 1; x < width-1; x++) {
            grid[y*width+x] = rand() > RAND_MAX/2;
        }
    }
}

void update_grid_cpu(bool* grid, bool* next_grid) {
    for (int y = 1; y < width-1; y++) {
        for (int x = 1; x < width-1; x++) {
            bool alive = grid[y*width+x];
            int neighborhood = 0;

            for (int dy = -1; dy < 2; dy++) {
                for (int dx = -1; dx < 2; dx++) {
                    if (dx == 0 && dy == 0) continue;
                    neighborhood += grid[(y + dy)*width + (x + dx)];
                }
            }

            next_grid[y*width+x] = (alive && neighborhood == 2) || neighborhood == 3;
        }
    }
}

__global__ void update_grid_gpu(bool* grid, bool* next_grid) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if (x == 0 || x == width-1 || y == 0 || y == width-1) return;

    bool alive = grid[y*width+x];
    int neighborhood = 0;

    for (int dy = -1; dy < 2; dy++) {
        for (int dx = -1; dx < 2; dx++) {
            if (dx == 0 && dy == 0) continue;
            neighborhood += grid[(y + dy)*width + (x + dx)];
        }
    }

    next_grid[y*width+x] = (alive && neighborhood == 2) || neighborhood == 3;
}

void save_grid(bool* grid) {
    FILE* file = fopen(gpu_mode ? "output_gpu.pgm" : "output_cpu.pgm", "w");
    fprintf(file, "P5 %d %d 1 ", width, width);
    fwrite(grid, width*width, 1, file);
    fclose(file);
    usleep(20 * 1000);
}

void print_grid(bool* grid) {
    for (int y = 0; y < width; y++) {
        for (int x = 0; x < width; x++) {
            printf(grid[y*width+x] ? "█" : "░");
        }
        printf("\n");
    }
}

//bool stop = false;
//void sigint_handler(int arg) {
//stop = true;
//}

int main(int argc, char** argv) {
    //signal(SIGINT, sigint_handler);

    bool* grid = new bool[width*width];
    bool* next_grid = new bool[width*width];
    init_grid(grid);
    init_grid(next_grid);

    bool* grid_gpu;
    bool* next_grid_gpu;
    cudaMalloc(&grid_gpu, width*width);
    cudaMalloc(&next_grid_gpu, width*width);
    cudaMemcpy(grid_gpu, grid, width*width, cudaMemcpyHostToDevice);
    cudaMemcpy(next_grid_gpu, next_grid, width*width, cudaMemcpyHostToDevice);

    for (int i = 0; i < 10; i++) {
        //if (gpu_mode) {
        //    cudaMemcpy(grid, grid_gpu, width*width, cudaMemcpyDeviceToHost);
        //}
        //print_grid(grid);

        clock_t start_time = clock();

        if (gpu_mode) {
            int block_width = 32;
            dim3 dim_grid(ceil((float)width/block_width), ceil((float)width/block_width));
            dim3 dim_block(block_width, block_width);

            update_grid_gpu<<<dim_grid, dim_block>>>(grid_gpu, next_grid_gpu);
            cudaDeviceSynchronize();
            swap(&grid_gpu, &next_grid_gpu);
        } else {
            update_grid_cpu(grid, next_grid);
            swap(&grid, &next_grid);
        }

        clock_t end_time = clock();
        printf("compute time : %f ms\n", (float)(end_time - start_time)/CLOCKS_PER_SEC*1000);

    }

    if (gpu_mode) {
        cudaMemcpy(grid, grid_gpu, width*width, cudaMemcpyDeviceToHost);
    }
    save_grid(grid);

    return 0;
}
