#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <time.h>
#include <chrono>
#include <cmath>
#include <signal.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <cuda_fp16.h>

#include "gifenc.h"

const bool output_to_display = true;
const bool output_to_gif = false;

const float outer_radius = 10;
const float inner_radius = outer_radius / 3;

const float birth_min = 0.257;
const float birth_max = 0.336;
const float survival_min = 0.365;
const float survival_max = 0.549;

const float alpha_n = 0.028;
const float alpha_m = 0.147;

const float time_step = 0.1;

const int universe_width = 512;
const int universe_size = universe_width * universe_width;
const int universe_bytes = universe_size * sizeof(float);

const int block_width = 32;

float* universe;
float* next_universe;

float* universe_gpu;
float* next_universe_gpu;

volatile bool stop_program = false;

void swap(void* a, void* b) {
    long* la = (long*)a;
    long* lb = (long*)b;

    long tmp = *la;
    *la = *lb;
    *lb = tmp;
}

__host__ __device__ float clampf(float value, float min, float max) {
    if (value < min) return min;
    if (value > max) return max;
    return value;
}

__host__ __device__ float clampf01(float value) {
    if (value < 0) return 0;
    if (value > 1) return 1;
    return value;
}

__host__ __device__ float circle_fraction(float circle_radius, float radius) {
    return clampf01(circle_radius + 0.5f - radius);
}

__host__ __device__ float circle_area(float radius) {
    return M_PI * radius * radius;
}

__host__ __device__ bool inside_circle(int cx, int cy, float r, int x, int y) {
    return (x - cx)*(x - cx) + (y - cy)*(y - cy) < r*r;
}

__host__ __device__ int wrap(int x) {
    return x < 0 ? x + universe_width : x % universe_width;
}

__host__ __device__ float sigma1(float x, float a, float alpha) {
    return 1.0f / (1.0f + expf(-(x - a) * 4.0f / alpha));
}

__host__ __device__ float sigma2(float x, float a, float b, float alpha) {
    return sigma1(x, a, alpha) * (1.0f - sigma1(x, b, alpha));
}

__host__ __device__ float sigmam(float x, float y, float m, float alpha) {
    return x * (1.0f - sigma1(m, 0.5, alpha)) + y * sigma1(m, 0.5, alpha);
}

__host__ __device__ float s(float n, float m) {
    //return sigma2(n, sigmam(birth_min, survival_min, m, alpha_m), sigmam(birth_max, survival_max, m, alpha_m), alpha_n);
    return sigmam(sigma2(n, birth_min, birth_max, alpha_n), sigma2(n, survival_min, survival_max, alpha_n), m, alpha_m);
}

float update_cpu() {
    for (int y = 0; y < universe_width; y++) {
        for (int x = 0; x < universe_width; x++) {
            float inner_area = 0;
            float outer_area = 0;

            float inner_area_fraction = 0;
            float outer_area_fraction = 0;

            for (int dy = -outer_radius; dy <= outer_radius; dy++) {
                for (int dx = -outer_radius; dx <= outer_radius; dx++) {
                    float radius = sqrtf(dx * dx + dy * dy);
                    float value = universe[wrap(y + dy) * universe_width + wrap(x + dx)];

                    float inner_fraction = circle_fraction(inner_radius, radius);
                    inner_area += inner_fraction;
                    inner_area_fraction += inner_fraction * value;

                    float outer_fraction = circle_fraction(outer_radius, radius) * circle_fraction(-inner_radius, -radius);
                    outer_area += outer_fraction;
                    outer_area_fraction += outer_fraction * value;
                }
            }

            inner_area_fraction /= inner_area;
            outer_area_fraction /= outer_area;

            float current_value = universe[y * universe_width + x];
            float new_value = clampf01(current_value + time_step * (s(outer_area_fraction, inner_area_fraction) * 2.0f - 1.0f));

            next_universe[y * universe_width + x] = new_value;
        }
    }

    swap(&universe, &next_universe);
    return 0;
}

__global__ void update_kernel(float* universe, float* next_universe) {
    int y = blockIdx.y * block_width + threadIdx.y;
    int x = blockIdx.x * block_width + threadIdx.x;

    if (y >= universe_width || x >= universe_width) return;

    const int tile_width = block_width + outer_radius * 2;
    __shared__ float tile[tile_width][tile_width];

    for (int by = 0; by < ceilf((float) tile_width / block_width); by++) {
        for (int bx = 0; bx < ceilf((float) tile_width / block_width); bx++) {
            int ty = by * block_width + threadIdx.y;
            int tx = bx * block_width + threadIdx.x;
            if (ty >= tile_width || tx >= tile_width) continue;
            int uy = blockIdx.y * block_width - outer_radius + ty;
            int ux = blockIdx.x * block_width - outer_radius + tx;
            tile[ty][tx] = universe[wrap(uy) * universe_width + wrap(ux)];
        }
    }

    float inner_area = 0;
    float outer_area = 0;

    float inner_area_fraction = 0;
    float outer_area_fraction = 0;

    for (int dy = -outer_radius; dy <= outer_radius; dy++) {
        for (int dx = -outer_radius; dx <= outer_radius; dx++) {
            float radius = sqrtf(dx * dx + dy * dy);
            float value = tile[threadIdx.y + dy + (int)ceilf(outer_radius)][threadIdx.x + dx + (int)ceilf(outer_radius)];
            //float value = universe[wrap(y + dy) * universe_width + wrap(x + dx)];

            float inner_fraction = circle_fraction(inner_radius, radius);
            inner_area += inner_fraction;
            inner_area_fraction += inner_fraction * value;

            float outer_fraction = circle_fraction(outer_radius, radius) * circle_fraction(-inner_radius, -radius);
            outer_area += outer_fraction;
            outer_area_fraction += outer_fraction * value;
        }
    }

    inner_area_fraction /= inner_area;
    outer_area_fraction /= outer_area;

    float current_value = universe[y * universe_width + x];
    float new_value = clampf01(current_value + time_step * (s(outer_area_fraction, inner_area_fraction) * 2.0f - 1.0f));

    next_universe[y * universe_width + x] = new_value;
}

float update_gpu() {
    const dim3 dim_block(block_width, block_width);
    const dim3 dim_grid(ceilf((float) universe_width / dim_block.x), ceilf((float) universe_width / dim_block.y));

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    update_kernel<<<dim_grid, dim_block>>>(universe_gpu, next_universe_gpu);

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    float kernel_time;
    cudaEventElapsedTime(&kernel_time, start, stop); 

    cudaError_t async_error = cudaDeviceSynchronize();
    cudaError_t sync_error = cudaGetLastError();
    if (async_error != cudaSuccess) printf("Kernel async error: %s\n", cudaGetErrorString(async_error));
    if (sync_error != cudaSuccess) printf("Kernel sync error: %s\n", cudaGetErrorString(sync_error));

    swap(&universe_gpu, &next_universe_gpu);
    cudaMemcpy(universe, universe_gpu, universe_bytes, cudaMemcpyDeviceToHost);

    return kernel_time;
}

void sigint_handler(int arg) {
    stop_program = true;
}

int main(int argc, char** argv) {
    srand(0);

    universe = new float[universe_size];
    next_universe = new float[universe_size];
    for (int y = 0; y < universe_width; y++) {
        for (int x = 0; x < universe_width; x++) {
            float f = (
                inside_circle(universe_width*0.4, universe_width*0.3, universe_width*0.1, x, y) ||
                inside_circle(universe_width*0.7, universe_width*0.6, universe_width*0.1, x, y) ||
                inside_circle(universe_width*0.3, universe_width*0.8, universe_width*0.1, x, y)) 
                ? 0.3 : 1;
            universe[y * universe_width + x] = clampf01(rand() / (float) RAND_MAX) * f;
            //universe[y * universe_width + x] = clampf(sinf((float)x/31) * cosf((float)y/31), 0, 1);
        }
    }
    cudaMalloc(&universe_gpu, universe_bytes);
    cudaMalloc(&next_universe_gpu, universe_bytes);
    cudaMemcpy(universe_gpu, universe, universe_bytes, cudaMemcpyHostToDevice);

    char* image = new char[universe_size * 4];
    for (int i = 0; i < universe_size * 4; i++) {
        image[i] = 0;
    }

	Display* display = XOpenDisplay(NULL);
	assert(display);
	Window window = XCreateSimpleWindow(display, XDefaultRootWindow(display), 0, 0, universe_width, universe_width, 0, 0, 0);
	XMapWindow(display, window);
	GC gc = XCreateGC(display, window, 0, NULL);
	XImage* ximage = XCreateImage(display, XDefaultVisual(display, XDefaultScreen(display)), 24, ZPixmap, 0, image, universe_width, universe_width, 8, 0);
    
    unsigned char gif_palette[256 * 3];
    for (int i = 0; i < 256; i++) {
        gif_palette[i * 3 + 0] = 0;
        gif_palette[i * 3 + 1] = i;
        gif_palette[i * 3 + 2] = 0;
    }
    ge_GIF* gif = ge_new_gif("smooth_life.gif", universe_width, universe_width, gif_palette, 8, -1, 0);

    /*
    cudaDeviceProp device_properties;
    cudaGetDeviceProperties(&device_properties, 0);
    printf("%s\n", device_properties.name);
    printf("%lu\n", device_properties.totalGlobalMem);
    printf("%lu\n", device_properties.sharedMemPerBlock);
    printf("%i\n", device_properties.maxThreadsPerBlock);
    printf("%i\n", device_properties.maxThreadsDim[0]);
    printf("%i\n", device_properties.maxThreadsDim[1]);
    printf("%i\n", device_properties.maxThreadsDim[2]);
    */

    /*
    int visual_infos_length;
    XVisualInfo* visual_infos = XGetVisualInfo(display, 0, NULL, &visual_infos_length);
    for (int i = 0; i < visual_infos_length; i++) {
        XVisualInfo visual_info = visual_infos[i];
        printf("VisualInfo %i, depth %i, bits_per_rgb %i\n", i, visual_info.depth, visual_info.bits_per_rgb);
    }
    return 0;
    */

    std::chrono::high_resolution_clock::time_point previous_frame_clock;

    signal(SIGINT, sigint_handler);

    for (int frame = 0; !stop_program; frame++) {
        if (output_to_display) {
            for (int i = 0; i < universe_size; i++) {
                image[i * 4 + 1] = universe[i] * 255;
            }
            XPutImage(display, window, gc, ximage, 0, 0, 0, 0, universe_width, universe_width);
            XFlush(display);
        }

        if (output_to_gif) {
            for (int i = 0; i < universe_size; i++) {
                gif->frame[i] = universe[i] * 255;
            }
            ge_add_frame(gif, 2);
        }

        float kernel_time = update_gpu();

        auto frame_clock = std::chrono::high_resolution_clock::now();
        float frame_time = std::chrono::duration_cast<std::chrono::nanoseconds>(frame_clock - previous_frame_clock).count() / 1000000000.0f;
        printf("frame %04i, kernel time %8.3f ms, frame time %8.3f ms (%4i fps)\n", frame, kernel_time, frame_time*1000, (int)(1/frame_time));
        previous_frame_clock = frame_clock;
    }

    if (output_to_gif) {
        ge_close_gif(gif);
    }

    return 0;
}

