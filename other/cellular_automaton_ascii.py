width = 50
grid = [[False for x in range(width)] for y in range(width)]

with open("input.txt", "rt") as file:
    for y, line in enumerate(file.readlines()):
        for x, c in enumerate(line):
            grid[y][x] = c == "X"

while input() != "q":
    for row in grid:
        for cell in row:
            print("X" if cell else ".", end="")
        print()
    
    next_grid = [[False for x in range(width)] for y in range(width)]

    for y in range(1, width-1):
        for x in range(1, width-1):
            neighborhood = 0
            for dy in range(-1, 2):
                for dx in range(-1, 2):
                    if dy == 0 and dx == 0: continue
                    neighborhood += grid[y+dy][x+dx]
            next_grid[y][x] = (grid[y][x] and neighborhood == 2) or (neighborhood == 3)
    
    grid = next_grid
