#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <ctime>

const int width = 1024;

bool* grid;
bool* next_grid;

bool stop = false;

void init_grid() {
    grid = new bool[width*width];
    next_grid = new bool[width*width];

    for (int y = 0; y < width; y++) {
        for (int x = 0; x < width; x++) {
            grid[y*width+x] = rand() > RAND_MAX/2;
        }
    }
}

void update_grid() {
    for (int y = 1; y < width-1; y++) {
        for (int x = 1; x < width-1; x++) {
            int neighborhood = 0;

            for (int dy = -1; dy < 2; dy++) {
                for (int dx = -1; dx < 2; dx++) {
                    if (dx == 0 && dy == 0) continue;
                    neighborhood += grid[(y + dy)*width + (x + dx)];
                }
            }

            next_grid[y*width+x] = ((grid[y*width+x] && neighborhood == 2) || neighborhood == 3);
        }
    }

    bool* tmp = grid;
    grid = next_grid;
    next_grid = tmp;
}

void save_grid() {
    FILE* file = fopen("output.pgm", "w");
    fprintf(file, "P5 %d %d 1 ", width, width);
    fwrite(grid, width*width, 1, file);
    fclose(file);
    usleep(20 * 1000);
}

void sigint_handler(int arg) {
    stop = true;
}

int main(int argc, char** argv) {
    init_grid();

    signal(SIGINT, sigint_handler);

    while (!stop) {
        save_grid();

        clock_t start_time = clock();
        update_grid();
        clock_t end_time = clock();
        printf("compute time : %f ms\n", (float)(end_time - start_time)/CLOCKS_PER_SEC*1000);
    }

    return 0;
}
