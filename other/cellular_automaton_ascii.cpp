#include <stdio.h>
#include <stdlib.h>

#include <wx-3.0/wx/wx.h>

class App : public wxApp {
    public:
        virtual bool OnInit() {
            return true;
        }
};

wxIMPLEMENT_APP(App);

// int main(int argc, char** argv) {
//     const int width = 500;
//     const int height = 100;

//     bool* grid = (bool*) malloc(width*height);
//     bool* next_grid = (bool*) malloc(width*height);

//     for (int y = 0; y < height; y++) {
//         for (int x = 0; x < width; x++) {
//             grid[y*width+x] = rand() > RAND_MAX/2;
//         }
//     }

    // FILE* file = fopen("input.txt", "rt");
    // int y = 0;
    // int x = 0;

    // while (true) {
    //     int c = fgetc(file);

    //     if (c == EOF) break;

    //     if (c == '\n') {
    //         y++;
    //         x = 0;
    //     } else {
    //         grid[y*width+x] = c == 'X';
    //         x++;
    //     }
    // }

    // do {
    //     for (int y = 1; y < height-1; y++) {
    //         for (int x = 1; x < width-1; x++) {
    //             printf(grid[y*width+x] ? "X" : ".");
    //         }
    //         printf("\n");
    //     }

    //     for (int y = 1; y < height-1; y++) {
    //         for (int x = 1; x < width-1; x++) {
    //             int neighborhood = 0;

    //             for (int dy = -1; dy < 2; dy++) {
    //                 for (int dx = -1; dx < 2; dx++) {
    //                     if (dx == 0 && dy == 0) continue;
    //                     neighborhood += grid[(y + dy)*width + (x + dx)];
    //                 }
    //             }

    //             next_grid[y*width+x] = ((grid[y*width+x] && neighborhood == 2) || neighborhood == 3);
    //         }
    //     }

    //     bool* tmp = grid;
    //     grid = next_grid;
    //     next_grid = tmp;
    // } while(getchar());

//     return 0;
// }
