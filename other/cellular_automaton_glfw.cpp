#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

const int width = 1200;
bool* grid;
bool* next_grid;

void init_grid() {
    grid = new bool[width*width];
    next_grid = new bool[width*width];

    for (int y = 0; y < width; y++) {
        for (int x = 0; x < width; x++) {
            grid[y*width+x] = rand() > RAND_MAX/2;
        }
    }
}

void update_grid() {
    for (int y = 1; y < width-1; y++) {
        for (int x = 1; x < width-1; x++) {
            int neighborhood = 0;

            for (int dy = -1; dy < 2; dy++) {
                for (int dx = -1; dx < 2; dx++) {
                    if (dx == 0 && dy == 0) continue;
                    neighborhood += grid[(y + dy)*width + (x + dx)];
                }
            }

            next_grid[y*width+x] = ((grid[y*width+x] && neighborhood == 2) || neighborhood == 3);
        }
    }

    bool* tmp = grid;
    grid = next_grid;
    next_grid = tmp;
}

uint compile_shader(uint type, const char* source) {
    uint shader = glCreateShader(type);

    glShaderSource(shader, 1, &source, nullptr);
    glCompileShader(shader);

    char log[1000];
    int log_length;
    glGetShaderInfoLog(shader, 1000, &log_length, log);

    if (log_length > 0) {
        printf("%s", log);
        assert(false);
    }

    return shader;
}

uint link_program(uint vertex_shader, uint fragment_shader) {
    uint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    char log[1000];
    int log_length;
    glGetProgramInfoLog(program, 1000, &log_length, log);

    if (log_length > 0) {
        printf("%s", log);
        assert(false);
    }

    return program;
}

int main(int argc, char** argv) {
    assert(glfwInit() == GLFW_TRUE);
    GLFWwindow* window = glfwCreateWindow(width, width, "Cellular Automaton", nullptr, nullptr);
    assert(window != nullptr);
    glfwMakeContextCurrent(window);
    assert(glewInit() == GLEW_OK);
    glfwSwapInterval(0);

    uint vertex_shader = compile_shader(GL_VERTEX_SHADER, 
    "#version 330\n"
    "out vec2 uv;\n"
    "void main() {\n"
    "   uv = vec2((gl_VertexID % 2), (((gl_VertexID + 1) / 3) % 2));\n"
    "   gl_Position = vec4(uv * 2 - 1, 0, 1);\n"
    "}\n");
    uint fragment_shader = compile_shader(GL_FRAGMENT_SHADER,
    "#version 330\n"
    "in vec2 uv;\n"
    "uniform sampler2D sampler;\n"
    "void main() {\n"
    "   gl_FragColor = bool(texture(sampler, uv).r) ? vec4(.28, .27, .24, 0) : vec4(.84, .82, .72, 0);\n"
    "}\n");
    uint program = link_program(vertex_shader, fragment_shader);
    glUseProgram(program);

    uint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, width, 0, GL_RED, GL_UNSIGNED_BYTE, nullptr);

    init_grid();

    while (!glfwWindowShouldClose(window)) {
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, width, GL_RED, GL_UNSIGNED_BYTE, grid);
        glClear(GL_COLOR_BUFFER_BIT);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        glfwSwapBuffers(window);
        glfwPollEvents();

        update_grid();
    }

    return 0;
}
