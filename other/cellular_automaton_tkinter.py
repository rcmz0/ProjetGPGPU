import numpy as np
from PIL import Image, ImageTk
import tkinter

# with Image.open("input.pbm") as image:
#     grid = np.array(image)

width = 200
grid = np.empty((width, width), bool)
next_grid = np.empty_like(grid)

window = tkinter.Tk()
label = tkinter.Label()
label.grid()

def callback(event=None):
    global grid, next_grid, bitmap

    height, width = grid.shape
    scale = 1

    image = Image.fromarray(grid).resize((width*scale, height*scale))
    bitmap = ImageTk.BitmapImage(image, background="#d5d2b8", foreground="#48453c")
    label["image"] = bitmap

    for y in range(1, height-1):
        for x in range(1, width-1):
            neighborhood = 0
            for dy in range(-1, 2):
                for dx in range(-1, 2):
                    if dy == 0 and dx == 0: continue
                    neighborhood += grid[y+dy][x+dx]
            next_grid[y][x] = (grid[y][x] and neighborhood == 2) or (neighborhood == 3)
    
    grid, next_grid = next_grid, grid

callback()
window.bind("<space>", callback)
window.mainloop()
