#include <stdio.h>
#include <stdlib.h>

#include <wx-3.0/wx/wx.h>

const int width = 100;
const int height = 100;

char* grid = (char*) malloc(width*height);
char* next_grid = (char*) malloc(width*height);

void init_grid() {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            grid[y*width+x] = rand() > RAND_MAX/2;
        }
    }
}

void update_grid() {
    for (int y = 1; y < height-1; y++) {
        for (int x = 1; x < width-1; x++) {
            int neighborhood = 0;

            for (int dy = -1; dy < 2; dy++) {
                for (int dx = -1; dx < 2; dx++) {
                    if (dx == 0 && dy == 0) continue;
                    neighborhood += grid[(y + dy)*width + (x + dx)];
                }
            }

            next_grid[y*width+x] = ((grid[y*width+x] && neighborhood == 2) || neighborhood == 3);
        }
    }

    char* tmp = grid;
    grid = next_grid;
    next_grid = tmp;
}

class Frame : public wxFrame {
    public:
        wxPanel* panel;

        Frame() : wxFrame(nullptr, -1, "Cellular automaton") {
            panel = new wxPanel(this);
            panel->Bind(wxEVT_KEY_DOWN, &Frame::OnKeyDown, this);
            panel->Bind(wxEVT_PAINT, &Frame::OnPaint, this);
            init_grid();
        }

        void OnKeyDown(wxKeyEvent& event) {
            update_grid();
            Refresh();
        }

        void OnPaint(wxPaintEvent& event) {
            wxPaintDC dc(panel);
            dc.SetUserScale(10, 10);

            wxPen foregroundPen(wxColour("#48453c"));
            wxPen backgroundPen(wxColour("#d5d2b8"));

            for (int y = 1; y < height-1; y++) {
                for (int x = 1; x < width-1; x++) {
                    dc.SetPen(grid[y*width+x] ? foregroundPen : backgroundPen);
                    dc.DrawRectangle(x, y, 1, 1);
                }
            }
        }
};

class App : public wxApp {
    public:
        virtual bool OnInit() {
            Frame* frame = new Frame();
            frame->Show();
            return true;
        }
};

wxIMPLEMENT_APP(App);
