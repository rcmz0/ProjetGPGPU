#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

int main(int argc, char** argv){
	int window_width = 600;
	int window_height = 400;

	Display* display = XOpenDisplay(NULL);
	assert(display);

	Window window = XCreateSimpleWindow(display, XDefaultRootWindow(display),
		0, 0, window_width, window_height, 0, 0, 0);
	XMapWindow(display, window);

	GC gc = XCreateGC(display, window, 0, NULL);

	XImage* image = XCreateImage(display, XDefaultVisual(display, XDefaultScreen(display)),
		24, ZPixmap, 0, malloc(window_width*window_height*4), window_width, window_height, 8, 0);

	for (int frame = 0; ; frame++) {
		for (int x = 0; x < window_width; x++) {
			for (int y = 0; y < window_height; y++) {
				int i = (y * window_width + x) * 4;
				image->data[i + 0] = x + frame;
			}
		}

		XPutImage(display, window, gc, image, 0, 0, 0, 0, window_width, window_height);
		XFlush(display);
		usleep(1000000 / 60);
	}
}
